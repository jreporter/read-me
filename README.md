# Jackie Porter's — README.md

I am a big fan of GitLab's 👁 [Transparency](https://about.gitlab.com/handbook/values/#transparency) value. I am a public by default-type of person and am excited to share with you how I work. This document was inspired by the [Product team README's](/handbook/product/readme/#product-readmes).

- [GitLab Profile](https://gitlab.com/jreporter)
- [Team Page](https://about.gitlab.com/company/team/#jreporter)
- [ReadMe Project](https://gitlab.com/jreporter/jreporter)

## Who I am 

- My friends and family call me Jack or Jacks. Everyone else calls me Jackie. Currently, I am a [Director, Product](https://about.gitlab.com/job-families/product/product-management-leadership/) for the [Verify](https://about.gitlab.com/direction/ops/#verify) and Package Stage.
- Happily, I call Weatherford, Texas home! I relocated from Austin, Texas where I attended the University of Texas at Austin to study engineering. 
- I share a house with my better other half, Justin, and my loving lab, [Piper](https://about.gitlab.com/company/team-pets/#240-pippa-piper). 
- I received my Masters in Business Administration from Texas State while working full-time as a Construction Manager. This was my first career that taught me so much about my love of building things. I am currently pursuing my Doctorate in Change Management.
- I was an athlete growing up and spend over a quarter of my awake time training for races, tournaments and (pre-COVID) boxing.

## How I work

- My timezone is [CST](https://time.is/CT)
- My days are non-linear 
- I will take calls between 7AM - 8PM, and will work between 5AM-10PM on any given day
- If I have tasks that take less than 5 minutes, I will do it immediately 
- I prioritize deep work in the mornings before 7AM 
- I have a strong bias for async communication
- I have `Do Not Book`, `Lunch`, `Dinner`, `Workouts`. If there is a block you are interested in for a customer call, please reach out!

## How I spend my time

- I spend the first three hours on Monday going through my calendar hygiene, filling in agendas, and declining or accepting meetings
- I spend one of the first hours of the day clearing out my Todos
- I spend the last hour of my work day clearing Todos, going to bed with a Todo is nerve-wracking for me
- I try to consolidate internal meetings on W, T and TH. I am always free for customer or vendor calls and will schedule those on any day
- I workout 6 days a week, I used to do two-a-day trainings and recently converted to longer one-a-day training in the afternoons 
- Mentoring, coaching, and engaging in thought leadership through events or platforms like Product Led Summit, Sharebird, and Topmate. 

## What I am focused on 

- At GitLab, I am most focused on scaling our product direction and vision to meet the enterprises where they need tools most for software development. I spend time in executive board reviews with account teams and customers, conducting strategic research, and connecting with my product leadership peers to effectively uplevel the business. 
- In my personal life, I am advancing my education in Change Management and Leadership, continuing education for project, product, and portfolio management certifications. I am also continually improving work-life balance to ensure I don't miss the important moments in my family's life. 

## How to reach me

- **Slack**: Mentioning me is the best way to reach me during the day (my handle is @jreporter). I will always have Slack open during business hours and will respond to `@` as soon as I see it. I do occasionally review other channels like #questions, #product, and #ops_section. If you need something - `@` me directly in a channel or link me to an issue in a DM. Sometimes I will set myself to away when I am in deep work. Expect my responses on email and Slack to be asynchronous in this state.
- **GitLab**: Mentioning me in issues and MRs is a failsafe way to get my fastest response. I am super active in burning my Todos down. 
- **[E-mail](mailto:jporter@gitlab.com)**: I read my emails 3 times a day, if you need something immediately or I am not responding quickly enough, please reach out on Slack. 

## Weak spots 

I am continuously improving and in FY25-Q4 I was told that I needed to continue: 

- Challenge the product teammates to achieve results for the customers 
- Demonstrating exceptional project management and deep ownership through clear timelines, risk management, and strong stakeholder alignment
- Being supportive and collaborative
- Cascade strategy and priorities for the business 
- Delegation of projects and tasks to my team, creating opportunities for growth

In my latest 360 feedback I received some comments to improve in the following:

- Displaying strong emotional reactions during disagreements - written or verbal. As this can make others unwilling to share their views. 
- Join in async discussions around cross-functional product scope like Hosted Runners for Dedicated to offer more expertise and decision-making. 
- Help teammates connect their personal goals and career development with company strategy 
- Address the lack of portfolio management in GitLab by setting clear, focused product priorities. 

In this most recent feedback session, I asked people who have worked with me most in the last 6 months, even including my day-to-day counterparts. This ended up creating a bias in the feedback, being more focused on cross-functional projects rather than product leadership. 

## Personality tests 

- My Myers-Briggs is [INTJ](https://www.16personalities.com/intj-personality) 
- My [Social Style](https://about.gitlab.com/handbook/leadership/emotional-intelligence/social-styles/#introducing-social-styles) is a **Driver**
- My [Insights](https://www.insights.com/us/) is a **Directing Reformer** leading with Red and Blue colors
- My [Strengths Finders Themes](https://www.gallup.com/cliftonstrengths/en/253715/34-cliftonstrengths-themes.aspx) are **Achiever, Analytical, Learner, Individualization, and Activator**
- My Enneagram is [Type 8](https://www.enneagraminstitute.com/type-8) The Challenger - The Powerful, Dominating Type: Self-Confident, Decisive, Willful, and Confrontational
